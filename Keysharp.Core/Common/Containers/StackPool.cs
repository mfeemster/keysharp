﻿namespace Keysharp.Core.Common.Containers
{
	/// <summary>
	/// Interface for an object which can be initialized and cleared.
	/// </summary>
	[PublicForTestOnly]
	public interface IClearable
	{
		/// <summary>
		/// Clears the object.
		/// </summary>
		void Clear();

		/// <summary>
		/// Initializes the object.
		/// </summary>
		void Init();
	}

	/// <summary>
	/// A wrapper class for a for an object pool which uses a <see cref="SlimStack"/> of objects of type <typeparamref name="T"/>.<br/>
	/// The intended usage is for the caller to rent an object from the pool, then return it when done.<br/>
	/// Since the underlying <see cref="SlimStack"/> object is thread-safe, so is this class, hence the prefix "Concurrent" in the name.<br/>
	/// The objects are all created in the constructor.
	/// </summary>
	/// <typeparam name="T"/>
	[PublicForTestOnly]
	public class ConcurrentStackPool<T>
		where T : class, IClearable, new ()
	{
		/// <summary>
		/// The <see cref="SlimStack"/> which holds the objects of type <typeparamref name="T"/>.
		/// </summary>
		private readonly SlimStack<T> collection;
		/// <summary>
		/// Gets the current index of the stack.
		/// </summary>
		public int Index => collection.Index;
		/// <summary>
		/// Initializes a new instance of the <see cref="ConcurrentStackPool"/> class.
		/// </summary>
		/// <param name="stackSize">The fixed size of the stack.</param>
		public ConcurrentStackPool(int stackSize)
		{
			collection = new (stackSize);

			for (var i = 0; i < stackSize; i++)
				_ = collection.Push(new T());
		}
		/// <summary>
		/// Rents an object from the stack.<br/>
		/// If an object was successfully rented, its <see cref="IClearable.Init"/> interface method will be called before returning.<br/>
		/// If there are no free elements in the stack, a new object is returned which
		/// is just allocated regularly on the heap.
		/// </summary>
		/// <returns>An object of type <typeparamref name="T"/>.</returns>
		public T Rent()
		{
			if (collection.TryPop(out var obj))
			{
				obj.Init();
				return obj;
			}

			return new T();
		}
		/// <summary>
		/// Returns an object to the stack and optionally clears it before returning by calling its <see cref="IClearable.Clear"/> interface method.
		/// </summary>
		/// <param name="val">The object to return.</param>
		/// <param name="clear">True to call the <see cref="IClearable.Clear"/> interface method before returning, else false.</param>
		/// <returns>True if returned, else false.</returns>
		public bool Return(T val, bool clear = true)
		{
			if (clear)
				val.Clear();

			return collection.Push(val);
		}
	}
}